FROM golang:1.14-alpine

ARG PORT=80

ENV PORT=$PORT

WORKDIR /app

COPY . .

RUN go mod tidy && go build -o /go/bin/app

ENTRYPOINT ["/go/bin/app"]
