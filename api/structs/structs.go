package structs

import (
	"image/color"
)

type RouteParams struct {
	Text    string
	Height  int
	Width   int
	Format  string
	FgColor color.Color
	BgColor color.Color
}
