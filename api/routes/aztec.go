package routes

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/aztec"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func AztecGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	minECCPctStr := r.FormValue("minECCPct")
	minECCPct := aztec.DEFAULT_EC_PERCENT

	if minECCPctStr != "" {
		minECCPct, err = strconv.Atoi(minECCPctStr)
		if err != nil {
			err = errors.New("'minECCPct' parameter must be an integer")
			return
		}
	}

	layersStr := r.FormValue("layers")
	layers := aztec.DEFAULT_LAYERS

	if layersStr != "" {
		layers, err = strconv.Atoi(layersStr)
		if err != nil {
			err = errors.New("'layers' parameter must be an integer")
			return
		}
	}

	code, err = aztec.Encode([]byte(params.Text), minECCPct, layers)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func AztecOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Default minECCPct is 33; default layers is 0"
	return
}
