package routes

import (
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"os"

	"github.com/boombuler/barcode"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

type coloredBarcode struct {
	barcode.Barcode
	cm color.Model
	fg color.Color
	bg color.Color
}

// TODO need an error return type
// TODO need a return type
func renderCode(code barcode.Barcode, params structs.RouteParams) (fileName string, err error) {

	// Scale bar code
	code, err = barcode.Scale(code, params.Width, params.Height)
	if err != nil {
		return "There was an error while scaling bar code", err
	}

	// Save PNG to a temp file
	file, err := ioutil.TempFile(os.TempDir(), "code")
	if err != nil {
		return "There was an error preparing file", err
	}
	defer file.Close()

	// Set up colors
	bc := NewColoredBarcode(code, color.RGBAModel, params.FgColor, params.BgColor)

	// Create final render
	if err := png.Encode(file, bc); err != nil {
		return "There was an error encoding final image", err
	}

	fileName = file.Name()

	if params.Format == "jpg" {
		pngFile, err := os.Open(file.Name())
		if err != nil {
			return "There was an error setting up jpg processing", err
		}
		defer pngFile.Close()
		pngSrc, err := png.Decode(pngFile)
		if err != nil {
			return "There was an error decoding png", err
		}
		dst := image.NewRGBA(pngSrc.Bounds())
		draw.Draw(dst, dst.Bounds(), image.NewUniform(params.BgColor), image.Point{}, draw.Src)
		draw.Draw(dst, dst.Bounds(), pngSrc, pngSrc.Bounds().Min, draw.Over)

		dstFile, err := ioutil.TempFile(os.TempDir(), "jpg")
		if err != nil {
			return "There was an error preparing file", err
		}
		defer dstFile.Close()
		err = jpeg.Encode(dstFile, dst, nil)
		if err != nil {
			return "There was an error encoding final jpg image", err
		}
		fileName = dstFile.Name()
	}

	return fileName, nil
}

func (bc *coloredBarcode) ColorModel() color.Model {
	return bc.cm
}
func (bc *coloredBarcode) At(x, y int) color.Color {
	c := bc.Barcode.At(x, y)
	if c == color.Black {
		return bc.fg
	} else {
		return bc.bg
	}
}

func NewColoredBarcode(bc barcode.Barcode, cm color.Model, foreground color.Color, background color.Color) barcode.Barcode {
	return &coloredBarcode{bc, cm, foreground, background}
}
