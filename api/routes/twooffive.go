package routes

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/twooffive"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func TwooffiveGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	interleavedParam := r.FormValue("interleaved")
	interleaved, err := strconv.ParseBool(interleavedParam)
	if err != nil {
		err = errors.New("Error with includeChecksum param; input must be a 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False.")
		return
	}

	code, err = twooffive.Encode(params.Text, interleaved)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func TwooffiveOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "interleaved can be true/false"
	return
}
