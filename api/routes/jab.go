package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/Littrell/barcode-api/api/structs"
)

func JabGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (out string) {
	fmt.Println("Heyo")
	out = "some output"
	return
}

func JabOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "some options string"
	return
}
