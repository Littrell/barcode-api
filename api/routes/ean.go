package routes

import (
	"errors"
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/ean"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func EanGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	// TODO this can return two error codes; can other types Encoding return multiple types? If so, handle this.
	code, err = ean.Encode(params.Text)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func EanOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Ean OPTIONS route"
	return
}
