package routes

import (
	"errors"
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/datamatrix"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func DatamatrixGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	code, err = datamatrix.Encode(params.Text)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func DatamatrixOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Datamatrix OPTIONS route"
	return
}
