package routes

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code39"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func Code39GET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	includeChecksumStr := r.FormValue("includeChecksum")
	includeChecksum, err := strconv.ParseBool(includeChecksumStr)
	if err != nil {
		err = errors.New("Error with includeChecksum param; input must be a 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False.")
		return
	}

	fullASCIIModeStr := r.FormValue("fullASCIIMode")
	fullASCIIMode, err := strconv.ParseBool(fullASCIIModeStr)
	if err != nil {
		err = errors.New("Error with includeChecksum param; input must be a 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False.")
	}

	code, err = code39.Encode(params.Text, includeChecksum, fullASCIIMode)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func Code39OPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "if include checksum is set to true, two checksum characters are calculated and added to the content"
	return
}
