package routes

import (
	"errors"
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func QrGET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode
	var errorCorrectionLevel qr.ErrorCorrectionLevel

	errorCorrectionLevelPct := r.FormValue("errorCorrectionLevelPct")

	if errorCorrectionLevelPct == "" || errorCorrectionLevelPct == "15" {
		errorCorrectionLevel = qr.M
	} else if errorCorrectionLevelPct == "7" {
		errorCorrectionLevel = qr.L
	} else if errorCorrectionLevelPct == "25" {
		errorCorrectionLevel = qr.Q
	} else if errorCorrectionLevelPct == "30" {
		errorCorrectionLevel = qr.H
	} else {
		err = errors.New("Error correct level percentage not supported. Must be either 7, 15, 25, or 30")
		return
	}

	/*
		qr.Auto is the Auto encoding type;
		this also allows for Numeric, AlphaNumeric, and Unicode
	*/
	code, err = qr.Encode(params.Text, errorCorrectionLevel, qr.Auto)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func QrOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "errorCorrectionLevelPct can be 7, 15, 25, or 30"
	return
}
