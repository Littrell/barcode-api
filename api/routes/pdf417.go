package routes

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/pdf417"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

func Pdf417GET(w http.ResponseWriter, r *http.Request, params structs.RouteParams) (fileName string, err error) {
	var code barcode.Barcode

	securityLevel := 0
	securityLevelParam := r.FormValue("securityLevel")
	if securityLevelParam != "" {
		securityLevel, err = strconv.Atoi(securityLevelParam)
		if err != nil {
			return
		}

		if securityLevel < 0 || securityLevel > 8 {
			err = errors.New("Security level must be between 0 and 8")
		}
	}

	securityLevelByte := byte(securityLevel)

	code, err = pdf417.Encode(params.Text, securityLevelByte)
	if err != nil {
		err = errors.New("There was an error while encoding")
		return
	}

	fileName, err = renderCode(code, params)
	if err != nil {
		err = errors.New("There was an error while rendering")
		return
	}

	return
}

func Pdf417OPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Pdf417 OPTIONS route"
	return
}
