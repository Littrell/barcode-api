package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"reflect"
)

////////////////////////
// UTILITY FUNCTIONS
////////////////////////

func RespondWithError(w http.ResponseWriter, code int, message string) {
	log.Printf("An error occurred - %d: %s", code, message)
	RespondWithJSON(w, code, map[string]string{"error": message})
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	log.Printf("Success - %d", code)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func StringInMap(key string, m map[string]interface{}) bool {
	if _, ok := m[key]; !ok {
		return false
	}
	return true
}

// This function expects the call-through function to return two values
// The first is a file name as a string and the second an error as type error
func Call(m map[string]interface{}, name string, params ...interface{}) (result []reflect.Value, err error) {
	f := reflect.ValueOf(m[name])
	if len(params) != f.Type().NumIn() {
		err = errors.New("The number of params is not adapted.")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	result = f.Call(in)
	// TODO better lock down this logic
	// Both OPTIONS and GET call this, and both of those
	// method types return a different number of arguments
	// Could force those functions to implement a interface?
	if len(result) == 2 {
		tmpError := result[1].Interface()
		if tmpError != nil {
			tmpErrorString := fmt.Sprintf("%v", tmpError)
			err = errors.New(tmpErrorString)
		}
	}
	return
}

func GetMapKeys(m map[string]interface{}) (strKeys []string) {
	for k := range m {
		strKeys = append(strKeys, k)
	}
	return
}
