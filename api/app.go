package api

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"image/color"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/Littrell/barcode-api/api/routes"
	"gitlab.com/Littrell/barcode-api/api/structs"
)

type App struct {
	router  *mux.Router
	get     map[string]interface{}
	options map[string]interface{}
}

func New() *App {
	a := new(App)
	a.router = mux.NewRouter()

	a.router.HandleFunc("/", a.Index).Methods("GET")
	a.router.HandleFunc("/types", a.Types).Methods("GET")
	a.router.HandleFunc("/types/{type}", a.TypeGET).Methods("GET")
	a.router.HandleFunc("/types/{type}", a.TypeOPTIONS).Methods("OPTIONS")

	// TODO might be able to use the constants here: https://godoc.org/github.com/boombuler/barcode
	// TODO use reflection to create this map
	a.get = map[string]interface{}{
		"qr":         routes.QrGET,
		"aztec":      routes.AztecGET,
		"datamatrix": routes.DatamatrixGET,
		"twooffive":  routes.TwooffiveGET,
		"codabar":    routes.CodabarGET,
		"ean":        routes.EanGET,
		"code128":    routes.Code128GET,
		"code39":     routes.Code39GET,
		"code93":     routes.Code93GET,
		"pdf417":     routes.Pdf417GET,
		"jab":        routes.JabGET,
	}

	// TODO use reflection to create this map
	a.options = map[string]interface{}{
		"qr":         routes.QrOPTIONS,
		"aztec":      routes.AztecOPTIONS,
		"datamatrix": routes.DatamatrixOPTIONS,
		"twooffive":  routes.TwooffiveOPTIONS,
		"codabar":    routes.CodabarOPTIONS,
		"ean":        routes.EanOPTIONS,
		"code128":    routes.Code128OPTIONS,
		"code39":     routes.Code39OPTIONS,
		"code93":     routes.Code93OPTIONS,
		"pdf417":     routes.Pdf417OPTIONS,
		"jab":        routes.JabOPTIONS,
	}

	return a
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.router))
}

///////////////////////////////////////////
// ROUTE HANDLERS
///////////////////////////////////////////

// GET /index
func (a *App) Index(w http.ResponseWriter, r *http.Request) {
	RespondWithJSON(w, 200, "Try /types...")
}

// OPTIONS /types/{type}
func (a *App) TypeOPTIONS(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	codeType := vars["type"]

	if codeType == "" {
		codeType = "qr"
	}

	if !StringInMap(codeType, a.get) {
		types := GetMapKeys(a.get)
		RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%s is an invalid type. Please provide one of the following types: %s", codeType, strings.Join(types, ", ")))
		return
	}

	funcReturns, _ := Call(a.options, codeType, w, r)
	options := funcReturns[0].String()

	RespondWithJSON(w, 200, options)
}

// GET /types/{type}
func (a *App) TypeGET(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	codeType := vars["type"]
	if codeType == "" {
		codeType = "qr"
	}

	if !StringInMap(codeType, a.get) {
		types := GetMapKeys(a.get)
		RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%s is an invalid type. Please provide one of the following types: %s", codeType, strings.Join(types, ", ")))
		return
	}

	text := r.FormValue("text")
	if text == "" {
		RespondWithError(w, http.StatusBadRequest, "Must provide text to convert!")
		return
	}

	width, err := strconv.Atoi(r.FormValue("width"))
	if err != nil {
		width = 200
	}

	height, err := strconv.Atoi(r.FormValue("height"))
	if err != nil {
		height = 200
	}

	var format string
	formatParam := r.FormValue("format")
	if formatParam != "" {
		if formatParam != "png" && formatParam != "jpg" {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("format provided %s is invalid. Must be 'png' or 'jpg'", formatParam))
			return
		}
		format = formatParam
	} else {
		format = "png"
	}

	var fgR uint8
	fgRParam := r.FormValue("fg_r")
	if fgRParam != "" {
		fgRInt, err := strconv.Atoi(fgRParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting fg_r with given value %s", fgRParam))
			return
		}
		if fgRInt < 0 || fgRInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("fg_r must be an integer between 0 and 255. %s provided", fgRInt))
			return
		}
		fgR = uint8(fgRInt)
	} else {
		fgR = 255
	}

	var fgG uint8
	fgGParam := r.FormValue("fg_g")
	if fgGParam != "" {
		fgGInt, err := strconv.Atoi(fgGParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting fg_g with given value %s", fgGParam))
			return
		}
		if fgGInt < 0 || fgGInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("fg_g must be an integer between 0 and 255. %s provided", fgGInt))
			return
		}
		fgG = uint8(fgGInt)
	} else {
		fgG = 255
	}

	var fgB uint8
	fgBParam := r.FormValue("fg_b")
	if fgBParam != "" {
		fgBInt, err := strconv.Atoi(fgBParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting fg_b with given value %s", fgBParam))
			return
		}
		if fgBInt < 0 || fgBInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("fg_b must be an integer between 0 and 255. %s provided", fgBInt))
			return
		}
		fgB = uint8(fgBInt)
	} else {
		fgB = 255
	}

	var fgA uint8
	fgAParam := r.FormValue("fg_a")
	if fgAParam != "" {
		fgAInt, err := strconv.Atoi(fgAParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting fg_a with given value %s", fgAParam))
			return
		}
		if fgAInt < 0 || fgAInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("fg_a must be an integer between 0 and 255. %s provided", fgAInt))
			return
		}
		fgA = uint8(fgAInt)
	} else {
		fgA = 255
	}

	var bgR uint8
	bgRParam := r.FormValue("bg_r")
	if bgRParam != "" {
		bgRInt, err := strconv.Atoi(bgRParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting bg_r with given value %s", bgRParam))
			return
		}
		if bgRInt < 0 || bgRInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("bg_r must be an integer between 0 and 255. %s provided", bgRInt))
			return
		}
		bgR = uint8(bgRInt)
	} else {
		bgR = 0
	}

	var bgG uint8
	bgGParam := r.FormValue("bg_g")
	if bgGParam != "" {
		bgGInt, err := strconv.Atoi(bgGParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting bg_g with given value %s", bgGParam))
			return
		}
		if bgGInt < 0 || bgGInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("bg_g must be an integer between 0 and 255. %s provided", bgGInt))
			return
		}
		bgG = uint8(bgGInt)
	} else {
		bgG = 0
	}

	var bgB uint8
	bgBParam := r.FormValue("bg_b")
	if bgBParam != "" {
		bgBInt, err := strconv.Atoi(bgBParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting bg_b with given value %s", bgBParam))
			return
		}
		if bgBInt < 0 || bgBInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("bg_b must be an integer between 0 and 255. %s provided", bgBInt))
			return
		}
		bgB = uint8(bgBInt)
	} else {
		bgB = 0
	}

	var bgA uint8
	bgAParam := r.FormValue("bg_a")
	if bgAParam != "" {
		bgAInt, err := strconv.Atoi(bgAParam)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue converting bg_a with given value %s", bgAParam))
			return
		}
		if bgAInt < 0 || bgAInt > 255 {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("bg_a must be an integer between 0 and 255. %s provided", bgAInt))
			return
		}
		bgA = uint8(bgAInt)
	} else {
		bgA = 255
	}

	foreGroundColor := color.RGBA{fgR, fgG, fgB, fgA}
	backGroundColor := color.RGBA{bgR, bgG, bgB, bgA}

	routeParams := structs.RouteParams{
		Text:    text,
		Height:  height,
		Width:   width,
		Format:  format,
		FgColor: foreGroundColor,
		BgColor: backGroundColor,
	}

	funcReturns, err := Call(a.get, codeType, w, r, routeParams)
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue processing this request. %s", err))
		return
	}
	filename := funcReturns[0].String()

	// Base64 encode, if specified
	var isBase64 bool
	base64Param := r.FormValue("base64")
	if base64Param != "" {
		isBase64, err = strconv.ParseBool(base64Param)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("Error with includeChecksum param; input must be a 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False."))
			return
		}
	} else {
		isBase64 = false
	}

	// Convert to base64 encoded string
	if isBase64 == true {
		// Check that the image exists
		image, err := os.Open(filename)
		if err != nil {
			RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue processing this request. Please contact the developer. %s", err))
			return
		}
		defer image.Close()

		imageInfo, _ := image.Stat()
		var size int64 = imageInfo.Size()
		imageBuffer := make([]byte, size)

		imageReader := bufio.NewReader(image)
		imageReader.Read(imageBuffer)
		imgBase64Str := base64.StdEncoding.EncodeToString(imageBuffer)
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write([]byte(fmt.Sprintf(imgBase64Str)))
	} else {
		// Send the temp file
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.ServeFile(w, r, filename)
	}

	// Remove temp file
	defer os.Remove(filename)

	return
}

func (a *App) Types(w http.ResponseWriter, r *http.Request) {
	types := GetMapKeys(a.get)
	RespondWithJSON(w, 200, types)
}
