package main

import (
	"fmt"
	"os"

	"gitlab.com/Littrell/barcode-api/api"
)

func main() {

	port := os.Getenv("PORT")

	fmt.Println("Running on port " + port)

	a := api.New()
	a.Run(":" + port)
}
