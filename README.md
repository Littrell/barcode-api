# Setup

```
docker-compose up -d
```

[http://localhost:3030/types](http://localhost:3030/types)

[http://localhost:3030/types/qr?text=hermleshlermle](http://localhost:3030/types/qr?text=hermleshlermle)

## Supported Codes

* qr
* aztec
* datamatrix
* code128
* code39
* jab
* twooffive
* codabar
* ean
* code93
* pdf417

## Take note

This API is under development and is likely to change.

# Resources

https://github.com/boombuler/barcode

